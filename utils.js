module.exports = {

    sleep : async function(msec) {
        return new Promise(resolve => setTimeout(resolve, msec));
    }
}