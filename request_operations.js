const axios = require("axios");

module.exports = {
    requestToApi: async (cityName) =>{
        try {
            let apiUrl = 'https://nominatim.openstreetmap.org/search';

            //let apiUrl ='https://nominatim.openstreetmap.org/search.php';

            return new Promise((resolveHead, reject) => {



            console.log("send request")


                axios.get(apiUrl, {
                    params: {
                        city: cityName,
                        polygon_geojson: 1,
                        format: "jsonv2",
                        country: "ru"


                    },

                    // https://nominatim.openstreetmap.org/search?city=%D0%BE%D0%BC%D1%81%D0%BA&country=ru&format=jsonv2&polygon_geojson=1
                    // https://nominatim.openstreetmap.org/search?city=мариуполь&country=ua&format=jsonv2&polygon_geojson=1
                    headers: {}
                })
                    .then(function (response) {

                        console.log("get response")

                        if (!response.data || (response.data && !response.data.length)) {
                            console.log(`город ${cityName} не существует или отсутствует в базе `
                            )
                            resolveHead("not isset city data")

                        }
                        else{
                            for(let item of response.data){
                                if((item.category === "place" && item.type === "city") ||
                                    (item.category === "boundary" && item.type === "administrative") ){

                                    console.log(item.category, item.type)

                                   return  resolveHead(JSON.stringify(item.geojson.coordinates));

                                }

                            }


                        }



                    })
                    .catch(function (error) {
                       console.log("Error: ", error);
                        reject()
                    })

            });

        }catch (e) {
            
        }

    }
}